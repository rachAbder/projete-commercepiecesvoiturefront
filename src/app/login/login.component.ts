import { LocalStorageService } from './../services/local-storage.service';


import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IClient } from '../model/Client';
import { AuthentificationService } from '../services/authentification.service';
import {IPanier} from "../model/Panier";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userForm!: FormGroup;
  user = { email: '', password: '' };
  message: string = '';
  panier?:IPanier = {lstPiece: [], prixTotal: 0}

  constructor(
    private fb: FormBuilder,
    private authService: AuthentificationService,
    private router: Router,
    private session: LocalStorageService
    ) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]

    })

  }

  // Informe l'utilisateur sur son authentfication.
  setMessage() {
    this.message = this.authService.isLoggedIn ? 'Vous êtes connecté.' : 'Identifiant ou mot de passe incorrect.';
  }

  login() {
    this.message = 'Tentative de connexion en cours ...';
    this.user = { ...this.userForm.value };
    this.authService.clientAuth(this.user.email, this.user.password).subscribe({
      next: (resp: IClient) => {

        console.log("Is Present ?", resp);
        this.setMessage();
        if (resp) {
          this.session.saveDataStorage('isLoggedIn', 'true');
          this.session.saveDataStorage('client', JSON.stringify(resp))

          if(localStorage.getItem('panier') === null && localStorage.getItem('nbArticles') === null ){
            this.session.saveDataStorage('panier', JSON.stringify(this.panier))
            this.session.saveDataStorage('nbArticles', JSON.stringify(0))
          }


          console.log("storage logedIn ?", this.session.getDataStorage('isLoggedIn'));
          let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '';
          this.router.navigate([redirect]);
        }
      }
    })

  }



}
