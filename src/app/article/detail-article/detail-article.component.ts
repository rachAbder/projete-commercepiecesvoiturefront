import { Component, OnInit } from '@angular/core';
import {ArticleService} from "../../services/article.service";
import {IPiece} from "../../model/Piece";
import {ActivatedRoute, ParamMap} from "@angular/router";

@Component({
  selector: 'app-detail-article',
  templateUrl: './detail-article.component.html',
  styleUrls: ['./detail-article.component.css']
})
export class DetailArticleComponent implements OnInit {
  ref?:string
  article?:IPiece
  constructor(private articleService : ArticleService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.ref = params.get('id')!
    })
    this.articleService.getArticle(this.ref!).subscribe(data => this.article = data);
  }

}
