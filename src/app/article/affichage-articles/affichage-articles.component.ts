import { Component, OnInit } from '@angular/core';
import {ArticleService} from "../../services/article.service";
import {IPiece} from "../../model/Piece";
import {IPanier} from "../../model/Panier";
import {Router} from "@angular/router";

@Component({
  selector: 'app-affichage-articles',
  templateUrl: './affichage-articles.component.html',
  styleUrls: ['./affichage-articles.component.css']
})
export class AffichageArticlesComponent implements OnInit {
  articles!:IPiece[];
  panier?:IPanier = {lstPiece: [], prixTotal: 0}

  constructor(private articleService : ArticleService, private router: Router) { }

  ngOnInit(): void {
    this.articleService.getAllArticles().subscribe(data => this.articles = data);
  }

  getArticle(ref: string): IPiece{
    let response: IPiece | undefined;
    this.articles.forEach( function (value) {
      if(value.ref === ref)response = value;
    })
    return <IPiece>response;
  }

  addPanier(ref: string){
    let article : IPiece = this.getArticle(ref);
    let panier : IPanier = JSON.parse(localStorage['panier']);
    let nbArticles : number = JSON.parse(localStorage['nbArticles']);
    panier.lstPiece.push(article);
    panier.prixTotal += article.tarif
    localStorage.setItem('panier', JSON.stringify(panier));
    localStorage.setItem('nbArticles', JSON.stringify(nbArticles+1));
  }

  detailArticle(ref: string){
    this.router.navigate(['/article/'+ref])
  }
}
