import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AffichageArticlesComponent } from "./affichage-articles.component";
import {HttpClientModule} from "@angular/common/http";
import {DataViewModule} from "primeng/dataview";
import {InputTextModule} from "primeng/inputtext";
import {ButtonModule} from "primeng/button";
import {FormsModule} from "@angular/forms";
import {RatingModule} from "primeng/rating";


@NgModule({
  declarations: [AffichageArticlesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    DataViewModule,
    InputTextModule,
    ButtonModule,
    FormsModule,
    RatingModule
  ]
})
export class AffichageArticlesModule { }
