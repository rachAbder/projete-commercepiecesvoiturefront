import {IPiece} from "./Piece";
import {IClient} from "./Client";

export interface ICommande{
  numeroCommande: string
  client: IClient
  pieces: IPiece[]
}
