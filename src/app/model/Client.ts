import { IAdresse } from "./Adresse";
import { ICommande } from "./Commande";
import { IPanier } from "./Panier";

export interface IClient {

    id : number;
    email : string;
    nom: string;
    prenom : string;
    password : string;
    panier : IPanier;
    adresse : IAdresse;
    version : number;
    commandes : ICommande[];
}