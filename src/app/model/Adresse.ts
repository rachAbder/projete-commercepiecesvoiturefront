export interface IAdresse{
    numero : number;
    rue : string;
    codePostale : number;
    ville : string;
}