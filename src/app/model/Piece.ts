import { ICommande } from "./Commande";

export interface IPiece{

    typePiece:string
    ref : string;
    nom : string;
    marque : string;
    description : string;
    tarif : number;
    img : string;
    commandes : ICommande[]
}
