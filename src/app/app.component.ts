

import {Component, OnInit} from '@angular/core';
import {IPanier} from "./model/Panier";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'projetVoitureFront';
  panier?:IPanier = {lstPiece: [], prixTotal: 0}

  ngOnInit(): void {
    if(localStorage.getItem('panier') === null && localStorage.getItem('nbArticles') === null ){
      localStorage.setItem('panier', JSON.stringify(this.panier))
      localStorage.setItem('nbArticles', JSON.stringify(0))
  }

}

}
