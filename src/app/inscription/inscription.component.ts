import { LocalStorageService } from './../services/local-storage.service';
import { Router } from '@angular/router';
import { IClient } from './../model/Client';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../services/authentification.service';


@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  inscripForm! : FormGroup;
  client! : IClient ;
  creationMessage : string = '';

  constructor(private fb : FormBuilder, 
    private authService : AuthentificationService, 
    private router : Router,
    private session: LocalStorageService,
    ) { }
  ngOnInit(): void {

    this.inscripForm = this.fb.group({
      email : ['', [Validators.required, Validators.email]],
      nom : ['', Validators.required],
      prenom : ['', Validators.required],
      password : ['', [Validators.required, Validators.minLength(10)]],
      adresse : this.fb.group({
        numero : ['', Validators.required],
        rue : ['', Validators.required],
        codePostale : ['', Validators.required],
        ville : ['', Validators.required],
      }),
    })
  }
  public register(){
    this.client = {...this.inscripForm.value};
    console.log(this.client);
    this.authService.clientRegistration(this.client).subscribe(
      
      (resp) =>{
        console.log(resp);
        if(resp){
          this.session.saveDataStorage('isLoggedIn', 'true');
          this.session.saveDataStorage('client', JSON.stringify(resp))
          alert("Vous êtes bien inscrit");
          this.router.navigate(['/'])
        }else{
          this.creationMessage = "Vous êtes déjà inscrit"
        }
      }
    )
  }

}
