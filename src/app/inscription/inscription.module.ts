import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InscriptionComponent } from './inscription.component';
import {CheckboxModule} from 'primeng/checkbox';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {PasswordModule} from 'primeng/password';
import {RippleModule} from "primeng/ripple";
@NgModule({
  declarations: [
    InscriptionComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    CheckboxModule,
    PasswordModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
    RippleModule
  ]
})
export class InscriptionModule { }
