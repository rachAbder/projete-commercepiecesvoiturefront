import { EditClientComponent } from './client/edit-client/edit-client.component';
import { AffichageClientsComponent } from './client/affichage-clients/affichage-clients.component';
import { FormsrGuard } from './guards/forms.guard';
import { InscriptionComponent } from './inscription/inscription.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AffichageArticlesComponent} from "./article/affichage-articles/affichage-articles.component";
import {DetailArticleComponent} from "./article/detail-article/detail-article.component";
import {AfficherPanierComponent} from "./panier/afficher-panier/afficher-panier.component";
import {AuthGuard} from "./guards/auth.guard";

const routes: Routes = [
  { path: 'articles', component: AffichageArticlesComponent},
  { path: 'article/:id', component: DetailArticleComponent},
  { path: 'panier', component: AfficherPanierComponent},
  { path : 'auth',
    children:[
      { path : 'login' , component : LoginComponent},
      { path : 'register' , component : InscriptionComponent},
   ]
  },
  { path: 'clients', component: AffichageClientsComponent},
  { path : 'clients/edit/:id', component : EditClientComponent},
  { path: '', redirectTo: 'articles', pathMatch: 'full'},
  { path: '**', redirectTo: 'articles', pathMatch: 'full'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
