import { IClient } from '../model/Client';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, Observable, of, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  URL: string = `http://localhost:8080`

  constructor(private http: HttpClient) { }

  isLoggedIn: boolean = false; // L'utilisateur est-il connecté ?
  redirectUrl?: string; // où rediriger l'utilisateur après l'authentification ?


  public clientAuth(email: string, password: string): Observable<IClient> {
    const queryParams = new HttpParams()
      .set('email', email)
      .set('password', password);
    /* const header = new HttpHeaders()
      .set('Access-Control-Allow-Origin','localhost:8080'); */
    console.log(queryParams.toString());
    let url = `${this.URL}/login`;
    return this.http.get<IClient>(url, { params: queryParams }).pipe(delay(1000))
  }

  public clientRegistration(client : IClient):Observable<IClient>{
    let url = `${this.URL}/clients/register`;
    return this.http.post<IClient>(url, client);
  }



   // Une méthode de déconnexion
   logout(): void {
    this.isLoggedIn = false;
  }

  closeLocalStorag(){
    if(localStorage.length != 0){
      localStorage.removeItem('isLoggedIn');
    }
  }

  }
