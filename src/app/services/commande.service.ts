import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { ICommande} from "../model/Commande";
import {Observable} from "rxjs";
import {IPiece} from "../model/Piece";

@Injectable({
  providedIn: 'root'
})
export class CommandeService {
  URL: string = `http://localhost:8080`
  constructor(private http: HttpClient) { }

  createCommande(commande: ICommande): Observable<ICommande>{
    const httpOptions = { headers: new HttpHeaders ({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'})
    };
    let url = `${this.URL}/commandes/create`;
    return this.http.post<ICommande>(url, commande, httpOptions);
  }
}
