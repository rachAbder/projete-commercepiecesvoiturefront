import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IPiece} from "../model/Piece";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  URL: string = `http://localhost:8080`
  constructor(private http: HttpClient) { }

  getAllArticles(): Observable<IPiece[]>{
    let url = `${this.URL}/articles/`;
    return this.http.get<IPiece[]>(url);
  }

  getArticle(ref: string): Observable<IPiece>{
    let url = `${this.URL}/articles/${ref}`;
    return this.http.get<IPiece>(url);
  }
}
