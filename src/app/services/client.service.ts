import { IClient } from './../model/Client';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  URL: string = `http://localhost:8080`

  constructor(private http : HttpClient) { }




  getAllClients():Observable<IClient[]>{
    let url = `${this.URL}/clients`
    return this.http.get<IClient[]>(url);

  }

  deletClientById(id : number){
    let url = `${this.URL}/clients/delete/${id}`;
    return this.http.delete(url)
  }

}
