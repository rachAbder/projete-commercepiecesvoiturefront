
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  public saveDataStorage(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  public getDataStorage(key: string) {
    let data = localStorage.getItem(key)|| "";
    return data;
  }
  

}
