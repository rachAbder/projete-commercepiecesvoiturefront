import { LocalStorageService } from '../services/local-storage.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class FormsrGuard implements CanActivate {

  constructor(private session: LocalStorageService, private router: Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean  {
    let url :string = state.url;
    return this.checkAccesToForms(url);
  }

  checkAccesToForms(url : string){
    if(this.session.getDataStorage('isLoggedIn') == "true"){
        this.router.navigate([url]);
        return false;
    }else{
      return true;
    }
  }
 
  
}
