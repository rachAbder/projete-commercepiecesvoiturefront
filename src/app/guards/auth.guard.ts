
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthentificationService } from '../services/authentification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService : AuthentificationService , private router: Router) { }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url :string = state.url;
    return this.checkLogin(url);
  }

  checkLogin(url : string){

    if(this.authService.isLoggedIn){
      return true;
    }
    this.authService.redirectUrl = url;
    this.router.navigate(['auth/login']);
    return false;
  }

}
