import { IClient } from './../../model/Client';
import { ClientService } from './../../services/client.service';
import { Component, OnInit } from '@angular/core';

import {TreeNode} from 'primeng/api';

@Component({
  selector: 'app-affichage-clients',
  templateUrl: './affichage-clients.component.html',
  styleUrls: ['./affichage-clients.component.css']
})
export class AffichageClientsComponent implements OnInit {

  

  cols!: any[];
  clients : IClient[] = []
  constructor(private clientService : ClientService) { }

  ngOnInit(): void {

    this.getAllClients()
    
  }

  deleteClient(id : number){
    this.clientService.deletClientById(id).subscribe(
      (resp) => {console.log(resp)
        this.getAllClients()
      }
    )
  }

  getAllClients(){
    this.clientService.getAllClients().subscribe(
      (clients) =>{ 
        this.clients = clients
      })
      
  }

}
