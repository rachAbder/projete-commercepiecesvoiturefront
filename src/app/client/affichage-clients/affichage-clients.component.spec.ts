import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageClientsComponent } from './affichage-clients.component';

describe('AffichageClientsComponent', () => {
  let component: AffichageClientsComponent;
  let fixture: ComponentFixture<AffichageClientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AffichageClientsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AffichageClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
