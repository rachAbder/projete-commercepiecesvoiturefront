import { ReactiveFormsModule } from '@angular/forms';



import { RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AffichageClientsComponent } from './affichage-clients/affichage-clients.component';


import {TableModule} from 'primeng/table';
import {TreeTableModule} from 'primeng/treetable';
import { EditClientComponent } from './edit-client/edit-client.component';

import {CheckboxModule} from 'primeng/checkbox';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';


import {PasswordModule} from 'primeng/password';
import {RippleModule} from "primeng/ripple";

@NgModule({
  declarations: [
    AffichageClientsComponent,
    EditClientComponent
  ],
    imports: [
        CommonModule,
        ButtonModule,
        TableModule,
        TreeTableModule,
        RouterModule,
        CheckboxModule,
        PasswordModule,
        ButtonModule,
        InputTextModule,
        ReactiveFormsModule,
        RippleModule
    ]
})
export class ClientModule { }
