import { LocalStorageService } from './../services/local-storage.service';
import { AuthentificationService } from './../services/authentification.service';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import { MenuItem } from "primeng/api";
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  items!: MenuItem[];
  items2!: MenuItem[];
  nbArticles: string = "0";

  constructor(
    private authService: AuthentificationService,
    private session: LocalStorageService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.items2 = [
      {
        label: 'Mon compte',
        routerLink: 'auth/login',
        visible : !this.visibility(),
      },
      {
        label: 'Déconnexion',
        icon: 'pi pi-fw pi-power-off',
        command: this.logout,
        visible : this.visibility(),
      },
    /*   {
        label: 'Connexion',
        icon: 'pi pi-fw pi-power-off',
        visible : !this.visibility(),
      }, */
    ];
  }




  getNbArticles(): string {
    return this.nbArticles = localStorage.getItem("nbArticles")!
  }


  // Déconnecte l'utilisateur
  logout = () => {
    this.authService.logout();
    this.authService.closeLocalStorag();
    this.router.navigate(['']);
  }

  visibility(): boolean{
    return this.session.getDataStorage('isLoggedIn') == "true";
  }

  afficherPanier(){
    this.router.navigate(['panier']);
  }

}
