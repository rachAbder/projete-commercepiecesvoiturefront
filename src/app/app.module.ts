import { ClientModule } from './client/client.module';


import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import { MenubarModule } from 'primeng/menubar';
import { AvatarModule } from 'primeng/avatar';
import { TieredMenuModule } from 'primeng/tieredmenu';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {BadgeModule} from "primeng/badge";
import { AffichageArticlesModule } from "./article/affichage-articles/affichage-articles.module";
import { DetailArticleComponent } from './article/detail-article/detail-article.component';
import {PasswordModule} from "primeng/password";
import { AfficherPanierComponent } from './panier/afficher-panier/afficher-panier.component';
import {ButtonModule} from "primeng/button";
import { InscriptionModule } from './inscription/inscription.module';
import { LoginModule } from './login/login.module';
import {TableModule} from "primeng/table";
import {RippleModule} from "primeng/ripple";
import {TagModule} from "primeng/tag";



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DetailArticleComponent,
    AfficherPanierComponent,

  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MenubarModule,
        AvatarModule,
        TieredMenuModule,
        BadgeModule,
        AffichageArticlesModule,
        InscriptionModule,
        LoginModule,
        ClientModule,
        ButtonModule,
        PasswordModule,
        TableModule,
        RippleModule,
        TagModule
    ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
