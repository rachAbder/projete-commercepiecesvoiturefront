import { Component, OnInit } from '@angular/core';
import {IPanier} from "../../model/Panier";
import {Router} from "@angular/router";
import {CommandeService} from "../../services/commande.service";
import {ICommande} from "../../model/Commande";
import {IPiece} from "../../model/Piece";
import {IClient} from "../../model/Client";


@Component({
  selector: 'app-afficher-panier',
  templateUrl: './afficher-panier.component.html',
  styleUrls: ['./afficher-panier.component.css']
})
export class AfficherPanierComponent implements OnInit {
  panier?:IPanier
  commande?:ICommande
  lstPiece!: IPiece[]
  constructor(private router: Router, private commandeService : CommandeService) { }

  ngOnInit(): void {
    this.panier =  JSON.parse(localStorage.getItem("panier")!)
  }

  createCommande(){
    let client: IClient = JSON.parse(localStorage.getItem("client")!)
    let commande : ICommande = {numeroCommande: "0", client: client , pieces: this.panier?.lstPiece! }

    this.commandeService.createCommande(commande).subscribe(data => this.commande = data)
    this.panier = {lstPiece: [], prixTotal: 0}
    localStorage.setItem('panier', JSON.stringify(this.panier))
    localStorage.setItem('nbArticles', JSON.stringify(0))
  }

  getPrixTotal(): number{
    this.panier =  JSON.parse(localStorage.getItem('panier')!)
    return <number>this.panier?.prixTotal
  }

  getPanier(): IPiece[]{
    return JSON.parse(localStorage.getItem('panier')!).lstPiece
  }

  deletePiecePanier(ref: string){
    for(var i = 0; i < this.panier?.lstPiece.length!; i++){
      if(this.panier?.lstPiece[i].ref == ref){
        this.panier.prixTotal = this.panier.prixTotal - <number>this.panier?.lstPiece[i].tarif
        this.panier.lstPiece.splice(i,1)
      }
    }
    localStorage.setItem('panier', JSON.stringify(this.panier))
    localStorage.setItem('nbArticles', JSON.stringify(JSON.parse(localStorage.getItem('nbArticles')!)-1));
  }

  checkPanierVide():boolean{
    let res : boolean = true
    if(JSON.parse(localStorage.getItem('nbArticles')!) > 0){
      res = false
    }
    return res
  }

}
